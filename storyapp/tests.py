from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.views import *
from story6_app.forms import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase
from .views import *
import unittest

class UnitTest(TestCase):
    def test_login_url_is_resolved(self):
        self.assertEqual(resolve(reverse('login')).func, loginView)
        
    def test_logged_url_is_resolved(self):
        url = reverse('logged')
        self.assertEqual(resolve(url).func, loggedView)
        
    def test_signup_url_is_resolved(self):
        url = reverse('signup')
        self.assertEqual(resolve(url).func, signUpView)
    
    def test_login_GET(self):
        response = Client().get(reverse('login'))
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'login.html')
    
    def test_logged_page_GET(self):
        response = Client().get(reverse('logged'))
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'logged.html')
     
    def test_signup_page_GET(self):
        response = Client().get(reverse('signup'))
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'singup.html')
        
    def test_add_sign_up_POST(self):
        response_post = Client().post('/signup/',
            {'username' : "bebe", 'password' : "baba"}
        )
        self.assertEqual(response_post.status_code, 302)
        
class Lab5FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super(Lab5FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.implicitly_wait(20)
        selenium.get('http://127.0.0.1:8000/signup/')
        # find the form element
        selenium.implicitly_wait(20)
        title = selenium.find_element_by_class_name('id_username').send_keys('syams.ramadan')
        title = selenium.find_element_by_class_name('id_password').send_keys('syims.rimidin')
        selenium.implicitly_wait(20)
        submit = selenium.find_element_by_id('submit')
        # Fill the form with data
        # submitting the form
        submit.send_keys(Keys.RETURN)
        selenium.implicitly_wait(20)  
        status = selenium.find_element_by_class_name('status').text
        self.assertIn("Hey There syams.ramadan", status)  
        selenium.implicitly_wait(20)